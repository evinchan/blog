
// 所有配置项说明请参见https://cli.vuejs.org/config/
module.exports = {
    lintOnSave: false, //关闭eslint检验
    publicPath: '/',
    outputDir: 'dist',
    assetsDir: 'static',
    // lintOnSave: process.env.NODE_ENV === 'development',
    productionSourceMap: false,
    devServer: {
        proxy: {
            '/api': {
                target: 'http://localhost:5220',//本地服务器
                ws: true,
                secure: true,
                changeOrigin: true,
                pathRewrite: {
                    '^/api': ''
                }
            }
        }
    },
}
